package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi12/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi12Client struct {
	pb.Gogi12Client
}

func NewGogi12Client(address string) *Gogi12Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi12 service", l.Error(err))
	}

	c := pb.NewGogi12Client(conn)

	return &Gogi12Client{c}
}
